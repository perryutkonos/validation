//Отключаем системные сообщения
document.forms[0].addEventListener('invalid', function (e) {
    e.preventDefault();
}, true);


var fields = document.querySelectorAll(".form input");
var validations = ['typeMismatch', 'tooShort', 'tooLong', 'valueMissing', 'badInput','rangeOverflow','rangeUnderflow','patternMismatch'];

//Для каждого поля устанавливаем обработчик на смену фокуса
for (i = 0; i < fields.length; i++) {

    var field = fields[i];
    field.addEventListener('blur', setValidate.bind(null,field));
}

//Отрабатываем нажатие
var button = document.getElementById("btn");
button.addEventListener('click', function (e) {
    
    e.preventDefault();
    if(formValid(fields)) {
        alert("Все гут!");
    }
    else alert("Все не гут(");
});


function setValidate(fieldBlock,event) {
    
    var isValid = true;
    for (j = 0; j < validations.length; j++) {

        if (fieldBlock.validity[validations[j]]) {
            setError(fieldBlock);
            isValid = false;
        }
    }
    if(isValid) deleteError(fieldBlock);
    
    return isValid;
}

function setError(field) {

    field.classList.remove("success");
    field.classList.add("error");
    var error = field.closest(".form__field").querySelectorAll(".form__error")[0];
    error.classList.add("active")
}

function deleteError(field) {
    
    field.classList.remove("error");
    field.classList.add("success");
    var error = field.closest(".form__field").querySelectorAll(".form__error")[0];
    error.classList.remove("active")
}

function formValid(fields){
    
    var isValid = true;
    for (i = 0; i < fields.length; i++) {

        var field = fields[i];
        isValid = setValidate(field);
    }
    
    return isValid;
}

(function() {

    // проверяем поддержку
    if (!Element.prototype.closest) {

        // реализуем
        Element.prototype.closest = function(css) {
            var node = this;

            while (node) {
                if (node.matches(css)) return node;
                else node = node.parentElement;
            }
            return null;
        };
    }

    // проверяем поддержку
    if (!Element.prototype.matches) {

        // определяем свойство
        Element.prototype.matches = Element.prototype.matchesSelector ||
            Element.prototype.webkitMatchesSelector ||
            Element.prototype.mozMatchesSelector ||
            Element.prototype.msMatchesSelector;

    }

})();